const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema;

const productSchema=new mongoose.Schema({
    title:{
        type:String,
        trim: true,
        required: true,
        maxlength: 20,
        unique: true
    },
    description:{
        type:String,
        required: true,
        maxlength: 2000,
    },
    price:{
        type:Number,
        trim: true,
        required: true,
        maxlength: 32,
    },
    discount:{
        type:Number,
        trim: true,
        default:0,
        maxlength: 32,
    },
    category:{
        type: ObjectId,
        ref: 'Category',
        required: true,
    },
    qty:{
        type:Number
    },
    photo:{
        data: Buffer,
        contentType: String
    },
    status:{
        required: false,
        type:Boolean
    }


},{timestamps: true});

module.exports = mongoose.model('Product', productSchema);