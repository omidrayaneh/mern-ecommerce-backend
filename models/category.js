const mongoose = require('mongoose');


const categorySchema=new mongoose.Schema({
    title:{
        type:String,
        trim: true,
        required: true,
        maxlength: 20,
        unique: true
    },


},{timestamps: true});

module.exports = mongoose.model('Category', categorySchema);